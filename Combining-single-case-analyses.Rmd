# Combining single case analyses {#csca}


## Replicated N = 1 designs

Single case studies are rarely limited to the study of only one subject. In fact, the statistical generalization would be poor if a study were limited to a single subject. Replication across participants may serve to improve statistical generalization. When more subjects are analyzed, the study becomes a replicated N = 1 study. Although the focus is still on change within the individual, replicated N = 1 studies offers the possibility to generalize the results to a larger population. Usually, the number of subjects in such designs are limited, for example 10 subjects are examined.  
As discussed in the introduction in some replicated designs the intervention moment is randomized across subjects. Such designs are called multiple baseline designs and are a way to generalize the intervention effect. Other randomizations which focus on the single subject are also possible. For example, a within subject variation of a multiple baseline design is to generalize over situations or to use different dependent variables.

In order to draw conclusions from replicated N = 1 studies, the results must be integrated to an overall measure of effect. The general idea behind the integration of the results from single case analyses is similar to conducting a meta analysis. One way to obtain an effect size and a statistical test is combining the p-values from randomization tests from the individual studies to an overall p-value [@edgington_randomization_2007; @manolov_comparing_2008]. 

There are various ways to combine the p-values, each with underlying assumptions. An overview with references to relevant software has been provided by @heyvaert_randomization_2014. A relatively easy and well-known method for obtaining a test statistic from several independent tests has been described by @fisher_statistical_1932. With $p_i$ the p-value from the $i^{th}$ single study, the statistic for the Fisher method with k different p-values is computed as:  

\begin{equation} 
X = -2 \sum_{i=1}^{k} \ln(P_{i})
(\#eq:Fisher)
\end{equation} 

The $X$ in \@ref(eq:Fisher) is following a $\chi^{2}_{2k}$ distribution with $\nu=2k$ degrees of freedom, from which a p-value for the overall hypothesis can easily be obtained. The function `pCombine` from `scda` computes this $\chi^{2}_{2k}$ value and its corresponding p-value. For example, we could combine the p-values of the `PAND` statistic for the six children in the Franco dataset [@franco_increasing_2013], see also @michiels_randomization_2020. These p-values can be obtained by the randomisation test. The 'Franco' data are available in the `scda` package.

```{r ExCombine, eval = TRUE, echo = TRUE, message = FALSE}

library(scda)
data("Franco")
pvalues <- NULL
for (i in 1:6) {
    res <- randomTestNonOverlap(data = subset(Franco, id == i), 
                                yVar = "score", 
                                phaseVar = "phase", 
                                minPhase = 3)
    
    pvalues <- c(pvalues,res$output$pvalues["PAND"])
}
pvalues
pCombine(pvalues)

```
Since the resulting p-value is smaller than 0.05, the conclusion from this analysis would be that the intervention across the six children seems effective, according to the `PAND` statistic. Similarly, one could test for the other nonoverlap statistics to see if the this conclusion is supported.

## Multilevel approach

Besides combining the effects from the non-parametric approach, with a model based approach such as the piecewise regression a more sophisticated approach can be used to combine various single case data. The replicated N = 1 data can be seen as hierarchical data, in which the measurements are nested within individuals. When there is an underlying statistical model, the single case data can be analyzed using the multilevel model. In this approach the data of all individuals are simultaneously analyzed, which yields effect sizes that can be seen as overall measures to evaluate an intervention. From such models several effect sizes can be obtained. 
The next chapters will discuss the multilevel approach to single case designs.

